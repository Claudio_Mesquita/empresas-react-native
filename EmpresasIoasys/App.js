import React from 'react';
import {NavigationContainer} from '@react-navigation/native';

import {AuthProvider} from './src/context/useAuth';
import {EnterpriseProvider} from './src/context/enterprisesContext';
import Routes from './src/screens';

export default () => (
  <NavigationContainer>
    <AuthProvider>
      <EnterpriseProvider>
        <Routes />
      </EnterpriseProvider>
    </AuthProvider>
  </NavigationContainer>
);
