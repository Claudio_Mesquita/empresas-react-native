import styled from 'styled-components/native';

const StyledContainer = styled.View`
  height: 50px;
  width: 100%;
  background-color: #c1c1c1;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-horizontal: 10px;
`;

const StyledView = styled.View`
  flex-direction: row;
  align-items: center;
`;

const StyledText = styled.Text`
  font-size: 22px;
  font-weight: 700;
`;

const StyledButton = styled.TouchableOpacity`
  align-items: center;
  margin-horizontal: 5px;
`;

export {StyledContainer, StyledText, StyledButton, StyledView};
