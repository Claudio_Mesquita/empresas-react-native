import React from 'react';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import {StyledContainer, StyledText, StyledButton, StyledView} from './styles';

export default ({title}) => {
  const navigation = useNavigation();

  const handleClickNavigation = local => {
    navigation.navigate(local);
  };

  return (
    <StyledContainer>
      <StyledView>
        {title != 'Empresas' && (
          <StyledButton onPress={() => handleClickNavigation('Enterprise')}>
            <Icon name="arrow-back" color="#000000" size={30} />
          </StyledButton>
        )}
        <StyledText>{title}</StyledText>
      </StyledView>

      {title == 'Empresas' && (
        <StyledView>
          <StyledButton onPress={() => handleClickNavigation('Filter')}>
            <Icon name="search" color="#000000" size={30} />
          </StyledButton>
          <StyledButton onPress={() => handleClickNavigation('User')}>
            <Icon name="person-pin" color="#000000" size={30} />
          </StyledButton>
        </StyledView>
      )}
    </StyledContainer>
  );
};
