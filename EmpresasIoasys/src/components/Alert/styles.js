import styled from 'styled-components/native';

const StyledContainer = styled.View`
  width: 100%;
  position: absolute;
  bottom: 20px;
`;

const StyledView = styled.View`
  width: 100%;
  padding-horizontal: 5px;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  border-width: 1px;
  border-color: #ffffff;
  border-radius: 10px;
  background-color: #c93f3f;
  padding-vertical: 15px;
  shadow-color: #000000;
  shadow-radius: 1px;
  elevation: 5;
`;

const StyledText = styled.Text`
  color: #ffffff;
  text-align: center;
  font-size: 15px;
  font-weight: 700;
  flex: 1;
`;

const StyledButton = styled.TouchableOpacity`
  padding-horizontal: 10px;
`;

export {StyledContainer, StyledView, StyledText, StyledButton};
