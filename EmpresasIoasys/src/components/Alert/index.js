import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {StyledContainer, StyledView, StyledText, StyledButton} from './styles';

export default ({messageError, exit}) => {
  const handleClickClose = () => {
    exit(false);
  };

  return (
    <StyledContainer>
      <StyledView>
        <StyledText>{messageError}</StyledText>
        <StyledButton onPress={handleClickClose}>
          <Icon name="close" color="#ffffff" size={27} />
        </StyledButton>
      </StyledView>
    </StyledContainer>
  );
};
