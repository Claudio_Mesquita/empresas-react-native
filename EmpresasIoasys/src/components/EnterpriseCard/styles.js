import styled from 'styled-components/native';

const StyledContainer = styled.View`
  width: 100%;
  border-width: 1px;
  border-color: #f1f1f1;
  border-radius: 5px;
  margin-vertical: 5px;
  background-color: #ffffff;
  shadow-color: #000000;
  shadow-radius: 1px;
  elevation: 5;
`;
const StyledTitleView = styled.View`
  padding: 5px;
  border-bottom-width: 1px;
  border-bottom-color: #f2f2f2;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
const StyledTitle = styled.Text`
  font-size: 18px;
  font-weight: 700;
`;

const StyledImageView = styled.View`
  width: 100%;
  height: 200px;
`;

const StyledDataView = styled.View`
  padding: 5px;
`;
const StyledText = styled.Text`
  font-style: italic;
  text-align: justify;
`;

const StyledButtonView = styled.View`
  border-top-width: 1px;
  border-top-color: #f1f1f1;
  padding-horizontal: 30px;
  padding-vertical: 10px;
`;

const StyledButton = styled.TouchableOpacity`
  width: 100%;
  padding-vertical: 15px;
  background-color: #000000;
  border-radius: 10px;
`;
const StyledButtonText = styled.Text`
  color: #ffffff;
  text-align: center;
  font-weight: bold;
`;

export {
  StyledContainer,
  StyledTitleView,
  StyledTitle,
  StyledImageView,
  StyledDataView,
  StyledText,
  StyledButtonView,
  StyledButton,
  StyledButtonText,
};
