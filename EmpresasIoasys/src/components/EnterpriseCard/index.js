import React, {useState} from 'react';
import {Animated, StyleSheet} from 'react-native';
import {
  StyledContainer,
  StyledTitleView,
  StyledTitle,
  StyledImageView,
  StyledDataView,
  StyledText,
  StyledButtonView,
  StyledButton,
  StyledButtonText,
} from './styles';

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '100%',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  imageOverlay: {
    position: 'absolute',
  },
});

import Details from '../Details';

export default ({data}) => {
  const [showModal, setShowModal] = useState(false);
  const defaultImageAnimated = new Animated.Value(0);
  const imageAnimated = new Animated.Value(0);

  const handleDefaultImageLoad = () => {
    Animated.timing(defaultImageAnimated, {
      toValue: 1,
      useNativeDriver: true,
    }).start();
  };

  const handleImageLoad = () => {
    Animated.timing(imageAnimated, {
      toValue: 1,
      useNativeDriver: true,
    }).start();
  };

  return (
    <StyledContainer>
      <StyledTitleView>
        <StyledTitle>{data?.enterprise_name}</StyledTitle>
        <StyledText>
          {data?.city}/{data?.country}
        </StyledText>
      </StyledTitleView>
      <StyledImageView>
        <Animated.Image
          source={require('../../assets/image-default.png')}
          style={[styles.image, {opacity: defaultImageAnimated}]}
          onLoad={handleDefaultImageLoad}
          onBlurRadius={1}
        />
        <Animated.Image
          source={{uri: `https://empresas.ioasys.com.br${data.photo}`}}
          style={[styles.image, {opacity: imageAnimated}, styles.imageOverlay]}
          onLoad={handleImageLoad}
        />
      </StyledImageView>
      <StyledDataView>
        <StyledText>
          {data.description.lenght < 100
            ? data.description
            : `${data.description.substring(0, 510)}`}
        </StyledText>
        <StyledText>Clique no botão abaixo para ver mais</StyledText>
      </StyledDataView>
      <StyledButtonView>
        <StyledButton onPress={() => setShowModal(true)}>
          <StyledButtonText>DETALHES</StyledButtonText>
        </StyledButton>
      </StyledButtonView>
      {showModal && (
        <Details show={showModal} setShow={setShowModal} data={data.id} />
      )}
    </StyledContainer>
  );
};
