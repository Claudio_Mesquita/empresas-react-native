import styled from 'styled-components/native';

const StyledView = styled.View`
  width: 100%;
`;

const StyledText = styled.Text`
  color: #000000;
`;

const StyledTextInputView = styled.View`
  width: 100%;
  padding-vertical: 5px;
  padding-horizontal: 10px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border-width: 1px;
  border-color: ${props => (props.focus == true ? '#000000' : '#f1f1f1')};
  border-radius: 10px;
  shadow-color: #000000;
  shadow-radius: 1px;
  elevation: 5;
  background-color: #ffffff;
`;
const StyledTextInput = styled.TextInput`
  padding-horizontal: 5px;
  padding-vertical: 7px;
  flex: 1;
`;

const StyledTextError = styled.Text`
  color: #c93f3f;
  margin-left: 2px;
`;

export {
  StyledView,
  StyledText,
  StyledTextInputView,
  StyledTextInput,
  StyledTextError,
};
