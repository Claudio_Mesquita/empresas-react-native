import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';

import {
  StyledTextInput,
  StyledView,
  StyledText,
  StyledTextInputView,
  StyledTextError,
} from './styles';

export default ({
  iconName = null,
  label = null,
  flex = 0,
  value,
  placeholder,
  onChangeText,
  secureTextEntry = false,
  error = null,
  keyboardType,
}) => {
  const [focus, setFocus] = useState(false);

  const onFocus = () => {
    setFocus(true);
  };

  const onBlur = () => {
    setFocus(false);
  };

  return (
    <StyledView>
      {label === null && <StyledText>{label}</StyledText>}

      <StyledTextInputView style={{flex}} focus={focus}>
        {iconName !== null && (
          <Icon name={iconName} size={25} color="#000000" />
        )}
        <StyledTextInput
          value={value}
          placeholder={placeholder}
          onChangeText={onChangeText}
          secureTextEntry={secureTextEntry}
          onFocus={() => onFocus()}
          onBlur={() => onBlur()}
          keyboardType={keyboardType}
        />
      </StyledTextInputView>
      {error && <StyledTextError>Esse campo é obrigatório!</StyledTextError>}
    </StyledView>
  );
};
