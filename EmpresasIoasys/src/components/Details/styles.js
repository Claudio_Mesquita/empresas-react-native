import styled from 'styled-components/native';

const StyledModal = styled.Modal``;
const StyledBackgroundView = styled.View`
  justify-content: flex-end;
  flex: 1;
  background-color: rgba(0, 0, 0, 0.2);
`;
const StyledView = styled.View`
  max-height: 99%;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  background-color: #ffffff;
  padding-bottom: 10px;
`;

const StyledTitleView = styled.View`
  padding-horizontal: 5px;
  padding-vertical: 10px;
  flex-direction: row;
  align-items: center;
  border-bottom-width: 1px;
  border-bottom-color: #f1f1f1;
`;
const StyledTitle = styled.Text`
  font-size: 21px;
  font-weight: bold;
  padding-left: 5px;
`;
const StyledButtonClose = styled.TouchableOpacity`
  padding-left: 5px;
`;
const StyledImageView = styled.View`
  width: 100%;
  height: 200px;
`;
const StyledImage = styled.Image`
  width: 100%;
  height: 100%;
`;

const StyledDataView = styled.View`
  padding-horizontal: 10px;
  border-top-width: 1px;
  border-top-color: #f1f1f1;
`;
const StyledTextBold = styled.Text`
  font-size: 16px;
  font-weight: bold;
`;
const StyledText = styled.Text`
  padding-vertical: 10px;
  padding-horizontal: 5px;
  font-style: italic;
  background-color: #f1f1f1;
  border-radius: 5px;
`;

const StyledScroll = styled.ScrollView``;
const StyledLoading = styled.ActivityIndicator``;

export {
  StyledModal,
  StyledBackgroundView,
  StyledView,
  StyledTitleView,
  StyledTitle,
  StyledImageView,
  StyledImage,
  StyledButtonClose,
  StyledDataView,
  StyledTextBold,
  StyledText,
  StyledScroll,
  StyledLoading,
};
