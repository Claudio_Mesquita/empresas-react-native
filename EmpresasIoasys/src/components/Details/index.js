import React, {useEffect, useState} from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';

import {
  StyledModal,
  StyledBackgroundView,
  StyledView,
  StyledTitleView,
  StyledTitle,
  StyledButtonClose,
  StyledImageView,
  StyledImage,
  StyledDataView,
  StyledTextBold,
  StyledText,
  StyledScroll,
  StyledLoading,
} from './styles';

import {enterpriseContext} from '../../context/enterprisesContext';
import Alert from '../Alert';

export default ({show, setShow, data}) => {
  const [mount, setMount] = useState(false);
  const {
    getDetailsEnterprise,
    detailsEnterprise,
    setDetailsEnterprise,
    loadingModal,
    setLoadingModal,
    messageError,
    setShowAlert,
    showAlert,
  } = enterpriseContext();

  useEffect(() => {
    if (mount == false) {
      getDetailsEnterprise(data);
      setMount(true);
    }
  }, [mount]);

  const handleClickClose = () => {
    setShow(false);
    setLoadingModal(true);
    setDetailsEnterprise(null);
  };

  return (
    <StyledModal
      transparent
      visible={show}
      animationType="slide"
      onRequestClose={handleClickClose}>
      <StyledBackgroundView>
        <StyledView>
          <StyledTitleView>
            <StyledButtonClose onPress={handleClickClose}>
              <Icon name="close" size={27} />
            </StyledButtonClose>
            <StyledTitle>Detalhes</StyledTitle>
          </StyledTitleView>
          {loadingModal ? (
            <StyledLoading size="large" color="#000000" />
          ) : (
            <StyledScroll>
              <StyledImageView>
                <StyledImage
                  source={{
                    uri: `https://empresas.ioasys.com.br${detailsEnterprise?.photo}`,
                  }}
                />
              </StyledImageView>
              <StyledDataView>
                <StyledTextBold>Empresa:</StyledTextBold>
                <StyledText>{detailsEnterprise?.enterprise_name}</StyledText>
                <StyledTextBold>E-mail:</StyledTextBold>
                <StyledText>{detailsEnterprise?.email_enterprise}</StyledText>
                <StyledTextBold>Localização:</StyledTextBold>
                <StyledText>
                  {detailsEnterprise?.city}/{detailsEnterprise?.country}
                </StyledText>
                <StyledTextBold>Tipo de empresa:</StyledTextBold>
                <StyledText>
                  {detailsEnterprise?.enterprise_type?.enterprise_type_name}
                </StyledText>
                <StyledTextBold>Valor:</StyledTextBold>
                <StyledText>{detailsEnterprise?.value}</StyledText>
                <StyledTextBold>Ações:</StyledTextBold>
                <StyledText>{detailsEnterprise?.shares}</StyledText>
                <StyledTextBold>Valor das ações:</StyledTextBold>
                <StyledText>{detailsEnterprise?.share_price}</StyledText>
                <StyledTextBold>Descrição:</StyledTextBold>
                <StyledText>{detailsEnterprise?.description}</StyledText>
              </StyledDataView>
            </StyledScroll>
          )}
        </StyledView>
      </StyledBackgroundView>
      {showAlert && <Alert messageError={messageError} exit={setShowAlert} />}
    </StyledModal>
  );
};
