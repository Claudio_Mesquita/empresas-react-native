import React, {createContext, useState, useEffect, useContext} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

import api from '../services/api';
const AuthContext = createContext({});

export const AuthProvider = ({children}) => {
  const [signed, setSigned] = useState(null);
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const [messageError, setMessageError] = useState('');

  const checkToken = async () => {
    if (signed == null) {
      const sign = await AsyncStorage.getItem('signed');
      const userInfo = await AsyncStorage.getItem('userData');
      setSigned(sign);
      setUser(userInfo);
      if (sign == null) {
        setSigned('logout');
      }
    }
  };

  useEffect(() => {
    checkToken();
  });

  const login = async data => {
    setLoading(true);
    try {
      const response = await api.post('/users/auth/sign_in', data);
      const accessToken = response.headers['access-token'];
      const {client} = response.headers;
      const {uid} = response.headers;

      await AsyncStorage.setItem('token', accessToken);
      await AsyncStorage.setItem('client', client);
      await AsyncStorage.setItem('uid', uid);

      api.defaults.headers['access-token'] = accessToken;
      api.defaults.headers.client = client;
      api.defaults.headers.uid = uid;

      await AsyncStorage.setItem('signed', 'signed');
      await AsyncStorage.setItem(
        'userData',
        JSON.stringify(response.data.investor),
      );

      setSigned('signed');
      setUser(response.data.investor);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      setShowAlert(true);
      if (JSON.stringify(error.message) === '"Network Error"') {
        setMessageError('A requisição falhou!!');
      } else {
        setMessageError('Usuário não encontrado! Tente novamente');
      }
      setSigned('logout');
      setUser(null);
    }
  };

  const logout = async () => {
    setSigned('logout');
    setUser(null);
    await AsyncStorage.setItem('signed', 'logout');
    await AsyncStorage.setItem('userData', toString(null));
  };

  return (
    <AuthContext.Provider
      value={{
        signed: signed,
        setSigned,
        user,
        login,
        logout,
        loading,
        showAlert,
        setShowAlert,
        messageError,
      }}>
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth() {
  const context = useContext(AuthContext);

  return context;
}
