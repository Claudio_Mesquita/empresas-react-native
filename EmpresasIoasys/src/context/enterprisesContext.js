import React, {createContext, useState, useEffect, useContext} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import api from '../services/api';

const EnterpriseContext = createContext({});

export const EnterpriseProvider = ({children}) => {
  const [listEnterprise, setListEnterprise] = useState([]);
  const [detailsEnterprise, setDetailsEnterprise] = useState();
  const [filterEnterprise, setFilterEnterprise] = useState();
  // loading
  const [loading, setLoading] = useState(false);
  const [loadingModal, setLoadingModal] = useState(true);
  const [showAlert, setShowAlert] = useState(false);
  const [messageError, setMessageError] = useState('');

  const getEnterprises = async () => {
    const token = await AsyncStorage.getItem('token');
    const client = await AsyncStorage.getItem('client');
    const uid = await AsyncStorage.getItem('uid');
    setLoading(true);
    try {
      const response = await api.get('/enterprises', {
        headers: {
          'access-token': token,
          client: client,
          uid: uid,
        },
      });
      setListEnterprise(response.data.enterprises);
      setLoading(false);
    } catch (error) {
      setShowAlert(true);
      setMessageError('Ocorreu um erro inesperado! Tente novamente!');
      setLoading(false);
    }
  };

  const getDetailsEnterprise = async data => {
    const token = await AsyncStorage.getItem('token');
    const client = await AsyncStorage.getItem('client');
    const uid = await AsyncStorage.getItem('uid');
    setLoadingModal(true);
    try {
      const response = await api.get(`/enterprises/${data}`, {
        headers: {
          'access-token': token,
          client: client,
          uid: uid,
        },
      });
      setDetailsEnterprise(response.data.enterprise);
      setLoadingModal(false);
    } catch (error) {
      setShowAlert(true);
      setMessageError('Ocorreu um erro inesperado! Tente novamente!');
      setLoadingModal(false);
    }
  };

  const getEnterpriseFilter = async data => {
    const token = await AsyncStorage.getItem('token');
    const client = await AsyncStorage.getItem('client');
    const uid = await AsyncStorage.getItem('uid');
    setLoading(true);
    try {
      const response = await api.get(
        `/enterprises?emterprise_types=${data.type}&name=${data.nome}`,
        {
          headers: {
            'access-token': token,
            client: client,
            uid: uid,
          },
        },
      );
      setFilterEnterprise(response.data.enterprises);
      if (response.data.enterprises == '') {
        setShowAlert(true);
        setMessageError('Nenhuma empresa localizada!');
      }
      setLoading(false);
    } catch (error) {
      setShowAlert(true);
      setMessageError('Ocorreu um erro inesperado! Tente novamente!');
      setLoading(false);
    }
  };

  return (
    <EnterpriseContext.Provider
      value={{
        getEnterprises,
        listEnterprise,
        loading,
        getDetailsEnterprise,
        detailsEnterprise,
        loadingModal,
        setDetailsEnterprise,
        setLoadingModal,
        getEnterpriseFilter,
        filterEnterprise,
        showAlert,
        setShowAlert,
        messageError,
        setFilterEnterprise,
      }}>
      {children}
    </EnterpriseContext.Provider>
  );
};

export function enterpriseContext() {
  const context = useContext(EnterpriseContext);

  return context;
}
