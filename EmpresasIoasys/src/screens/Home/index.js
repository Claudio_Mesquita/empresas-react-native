import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Enterprise from './Enterprises';
import User from './User';
import Filter from './Filter';

const Stack = createStackNavigator();

export default () => (
  <Stack.Navigator
    initialRouteName="Enterprise"
    screenOptions={{
      headerShown: false,
    }}>
    <Stack.Screen name="Enterprise" component={Enterprise} />
    <Stack.Screen name="User" component={User} />
    <Stack.Screen name="Filter" component={Filter} />
  </Stack.Navigator>
);
