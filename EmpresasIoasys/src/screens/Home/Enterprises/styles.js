import styled from 'styled-components/native';

const StyledContainer = styled.View`
  flex: 1;
  background-color: #ffffff;
`;

const StyledView = styled.View`
  width: 100%;
  flex: 1;
  padding: 15px;
`;

const StyledList = styled.FlatList``;
const StyledLoading = styled.ActivityIndicator`
  align-self: center;
`;

export {StyledContainer, StyledView, StyledList, StyledLoading};
