import React, {useEffect, useState} from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';

import {StyledContainer, StyledView, StyledList, StyledLoading} from './styles';

import {enterpriseContext} from '../../../context/enterprisesContext';
import Header from '../../../components/Header';
import EnterpriseCard from '../../../components/EnterpriseCard';
import Alert from '../../../components/Alert';

export default () => {
  const {
    getEnterprises,
    loading,
    listEnterprise,
    messageError,
    showAlert,
    setShowAlert,
  } = enterpriseContext();

  const [mount, setMount] = useState(false);

  useEffect(() => {
    if (!mount) {
      getEnterprises();
      setMount(true);
    }
  }, [mount]);

  return (
    <StyledContainer>
      <Header title="Empresas" />
      <StyledView>
        {loading ? (
          <StyledLoading size="large" color="#000000" />
        ) : (
          <StyledList
            data={listEnterprise}
            renderItem={({item}) => <EnterpriseCard data={item} />}
            keyExtractor={item => item.id}
            showsVerticalScrollIndicator={false}
          />
        )}
      </StyledView>
      {showAlert && <Alert messageError={messageError} exit={setShowAlert} />}
    </StyledContainer>
  );
};
