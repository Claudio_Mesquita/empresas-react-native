import styled from 'styled-components/native';

const StyledContainer = styled.View`
  flex: 1;
  background-color: #ffffff;
`;
const StyledSearchView = styled.View`
  padding-horizontal: 35px;
  background-color: #f1f1f1;
  border-bottom-width: 1px;
  border-bottom-color: #f1f1f1;
  padding-bottom: 10px;
`;
const StyledDataView = styled.View`
  height: 100%;
  padding-vertical: 10px;
  padding-horizontal: 10px;
`;

const StyledTitle = styled.Text`
  text-align: center;
  font-size: 20px;
  font-weight: bold;
`;

const StyledText = styled.Text``;

const StyledButton = styled.TouchableOpacity`
  width: 100%;
  margin-top: 10px;
  padding-vertical: 15px;
  background-color: #000000;
  border-radius: 15px;
`;
const StyleButtonText = styled.Text`
  font-weight: bold;
  color: #ffffff;
  font-size: 15px;
  text-align: center;
`;

const StyledLoading = styled.ActivityIndicator``;

const StyledList = styled.FlatList``;

export {
  StyledContainer,
  StyledSearchView,
  StyledDataView,
  StyledTitle,
  StyledText,
  StyledButton,
  StyleButtonText,
  StyledLoading,
  StyledList,
};
