import React, {useState} from 'react';
import {useForm, Controller} from 'react-hook-form';
import {
  StyledContainer,
  StyledSearchView,
  StyledDataView,
  StyledTitle,
  StyledText,
  StyledButton,
  StyleButtonText,
  StyledLoading,
  StyledList,
} from './styles';

import {enterpriseContext} from '../../../context/enterprisesContext';
import Header from '../../../components/Header';
import TextInput from '../../../components/TextInput';
import EnterpriseCard from '../../../components/EnterpriseCard';
import Alert from '../../../components/Alert';

export default () => {
  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm();

  const {
    loading,
    getEnterpriseFilter,
    filterEnterprise,
    showAlert,
    messageError,
    setShowAlert,
  } = enterpriseContext();

  const handleClickSearch = data => {
    getEnterpriseFilter(data);
  };

  return (
    <StyledContainer>
      <Header title="Pesquisar" />
      <StyledSearchView>
        <StyledTitle>Pesquisar Empresa</StyledTitle>
        <StyledText>Nome da empresa:</StyledText>
        <Controller
          control={control}
          rules={{required: true}}
          render={({field: {onChange, value}}) => (
            <TextInput
              label="Nome da empresa:"
              onChangeText={onChange}
              value={value}
              error={errors.nome?.type}
            />
          )}
          name="nome"
          defaultValue=""
        />

        <StyledText>Tipo da empresa:</StyledText>
        <Controller
          control={control}
          rules={{required: true}}
          render={({field: {onChange, value}}) => (
            <TextInput
              label="Tipo da empresa:"
              onChangeText={onChange}
              value={value}
              error={errors.tipo?.type}
              keyboardType="numeric"
            />
          )}
          name="tipo"
          defaultValue=""
        />
        <StyledButton onPress={handleSubmit(handleClickSearch)}>
          {loading ? (
            <StyledLoading size="small" color="#ffffff" />
          ) : (
            <StyleButtonText>PESQUISAR</StyleButtonText>
          )}
        </StyledButton>
      </StyledSearchView>
      <StyledDataView>
        <StyledList
          data={filterEnterprise}
          renderItem={({item}) => <EnterpriseCard data={item} />}
          keyExtractor={item => item.id}
          showsVerticalScrollIndicator={false}
        />
      </StyledDataView>
      {showAlert && <Alert messageError={messageError} exit={setShowAlert} />}
    </StyledContainer>
  );
};
