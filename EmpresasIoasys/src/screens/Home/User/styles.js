import styled from 'styled-components/native';

const StyledContainer = styled.View`
  flex: 1;
`;
const StyledImageView = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: #f1f1f1;
`;
const StyledImage = styled.Image`
  width: 100%;
  height: 100%;
`;

const StyledProfileView = styled.View`
  flex: 2;
  background-color: #ffffff;
  padding-horizontal: 10px;
  border-top-width: 1px;
  border-top-color: #f1f1f1;
  border-bottom-width: 1px;
  border-bottom-color: #f1f1f1;
`;
const StyledTitle = styled.Text`
  text-align: center;
  font-size: 18px;
  font-weight: bold;
`;

const StyledTextBold = styled.Text`
  font-size: 16px;
  font-weight: bold;
`;
const StyledText = styled.Text`
  padding-vertical: 10px;
  padding-horizontal: 5px;
  font-style: italic;
  background-color: #f1f1f1;
  border-radius: 5px;
`;

const StyledDataView = styled.View`
  flex: 2;
  background-color: #ffffff;
  padding-horizontal: 10px;
  border-bottom-width: 1px;
  border-bottom-color: #f1f1f1;
`;

const StyledButtonView = styled.View`
  flex: 1;
  background-color: #ffffff;
  justify-content: center;
  padding-horizontal: 40px;
`;
const StyledButton = styled.TouchableOpacity`
  width: 100%;
  padding-vertical: 15px;
  background-color: #000000;
  border-radius: 15px;
`;
const StyledButtonText = styled.Text`
  font-weight: bold;
  color: #ffffff;
  font-size: 15px;
  text-align: center;
`;

export {
  StyledContainer,
  StyledProfileView,
  StyledImage,
  StyledTitle,
  StyledTextBold,
  StyledText,
  StyledImageView,
  StyledDataView,
  StyledButtonView,
  StyledButton,
  StyledButtonText,
};
