import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  StyledContainer,
  StyledProfileView,
  StyledImage,
  StyledTitle,
  StyledTextBold,
  StyledText,
  StyledImageView,
  StyledDataView,
  StyledButtonView,
  StyledButton,
  StyledButtonText,
} from './styles';

import {useAuth} from '../../../context/useAuth';
import Header from '../../../components/Header';

export default () => {
  const {user, logout} = useAuth();

  const handleClickSair = () => {
    logout();
  };

  return (
    <StyledContainer>
      <Header title="Perfil" />

      <StyledImageView>
        {user?.photo != null ? (
          <StyledImage
            source={{uri: `https://empresas.ioasys.com.br${user.photo}`}}
            resizeMode="contain"
          />
        ) : (
          <Icon name="person" size={80} />
        )}
      </StyledImageView>
      <StyledProfileView>
        <StyledTitle>DADOS</StyledTitle>
        <StyledTextBold>Nome:</StyledTextBold>
        <StyledText>{user?.investor_name}</StyledText>
        <StyledTextBold>E-mail:</StyledTextBold>
        <StyledText>{user?.email}</StyledText>
        <StyledTextBold>Cidade/País</StyledTextBold>
        <StyledText>
          {user?.city}/{user?.country}
        </StyledText>
      </StyledProfileView>
      <StyledDataView>
        <StyledTitle>PORTFOLIO</StyledTitle>
        <StyledTextBold>Total de empresas:</StyledTextBold>
        <StyledText>{user?.portfolio?.enterprises_number}</StyledText>
        <StyledTextBold>Valor do portfolio:</StyledTextBold>
        <StyledText>{user?.portfolio_value}</StyledText>
        <StyledTextBold>Saldo</StyledTextBold>
        <StyledText>{user?.balance}</StyledText>
      </StyledDataView>

      <StyledButtonView>
        <StyledButton onPress={handleClickSair}>
          <StyledButtonText>SAIR</StyledButtonText>
        </StyledButton>
      </StyledButtonView>
    </StyledContainer>
  );
};
