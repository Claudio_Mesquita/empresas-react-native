import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {useAuth} from '../context/useAuth';

import Authentication from './Authentication';
import Home from './Home';

const Stack = createStackNavigator();

export default () => {
  const {signed} = useAuth();

  return (
    <Stack.Navigator
      initialRouteName="Authentication"
      screenOptions={{
        headerShown: false,
      }}>
      {signed != 'signed' ? (
        <Stack.Screen name="Authentication" component={Authentication} />
      ) : (
        <Stack.Screen name="Home" component={Home} />
      )}
    </Stack.Navigator>
  );
};
