import React from 'react';
import {useForm, Controller} from 'react-hook-form';
import {
  StyledContainer,
  StyledLogo,
  StyledView,
  StyledText,
  StyledButtonView,
  StyledButton,
  StyledButtonText,
  StyledLoading,
} from './styles';

import {useAuth} from '../../../context/useAuth';
import TextInput from '../../../components/TextInput';
import Alert from '../../../components/Alert';

export default () => {
  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm();

  const {login, loading, showAlert, setShowAlert, messageError, signed} =
    useAuth();

  const handleClickSignIn = data => {
    login(data);
  };

  return (
    <StyledContainer>
      <StyledLogo source={require('../../../assets/logo_ioasys.png')} />
      {signed == 'logout' ? (
        <StyledView>
          <StyledText>LOGIN</StyledText>
          <Controller
            control={control}
            rules={{required: true}}
            render={({field: {onChange, value}}) => (
              <TextInput
                iconName="email"
                placeholder="E-mail"
                onChangeText={onChange}
                value={value}
                error={errors.email?.type}
              />
            )}
            name="email"
            defaultValue=""
          />
          <Controller
            control={control}
            rules={{required: true}}
            render={({field: {onChange, value}}) => (
              <TextInput
                iconName="lock"
                placeholder="Password"
                onChangeText={onChange}
                value={value}
                secureTextEntry
                error={errors.password?.type}
              />
            )}
            name="password"
            defaultValue=""
          />

          <StyledButtonView>
            <StyledButton onPress={handleSubmit(handleClickSignIn)}>
              {loading ? (
                <StyledLoading color="#ffffff" size="small" />
              ) : (
                <StyledButtonText>LOGIN</StyledButtonText>
              )}
            </StyledButton>
          </StyledButtonView>
        </StyledView>
      ) : (
        <StyledView>
          <StyledLoading size="large" color="#000000" />
          <StyledText>Verificando Token</StyledText>
        </StyledView>
      )}

      {showAlert && <Alert messageError={messageError} exit={setShowAlert} />}
    </StyledContainer>
  );
};
