import styled from 'styled-components/native';

const StyledContainer = styled.View`
  background-color: #c1c1c1;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const StyledLogo = styled.Image`
  height: 320px;
  width: 55%;
  resize-mode: contain;
  flex: 1;
`;

const StyledView = styled.View`
  width: 100%;
  padding-horizontal: 25px;
  padding-top: 30px;
  align-items: center;
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
  background-color: #ffffff;
  flex: 1;
`;

const StyledText = styled.Text`
  font-size: 20px;
  font-weight: bold;
  letter-spacing: 1px;
`;

const StyledButtonView = styled.View`
  width: 100%;
  padding-horizontal: 30px;
`;

const StyledButton = styled.TouchableOpacity`
  width: 100%;
  margin-top: 25px;
  padding-vertical: 15px;
  background-color: #000000;
  border-radius: 10px;
`;
const StyledButtonText = styled.Text`
  font-size: 15px;
  text-align: center;
  font-weight: 700;
  color: #ffffff;
`;

const StyledLoading = styled.ActivityIndicator``;

export {
  StyledContainer,
  StyledLogo,
  StyledText,
  StyledView,
  StyledButton,
  StyledButtonText,
  StyledButtonView,
  StyledLoading,
};
