# Desafio React Native - ioasys

O desafio proposto pela Ioasys, em desenvolver uma aplicação React Native, foi realizado. O descritivo original foi movido para dentro da pasta EmpresasIoasys e está disponível no arquivo README.md.

# Executar Projeto

cd EmpresasIoasys/
yarn ou npm install

- Android
- npx react-native run-android

**Aviso para IOS**

- Por ter apenas o sistema linux, não foi possível configurar as libs para IOS corretamente, por esse motivo, é provável conter erros!
- IOS
  - cd ios/
  - pod install
  - cd ..
  - npx react-native run-ios

# Bibliotecas Utilizadas

- @react-native-async-storage/async-storage: Biblioteca utilizada para salvar os dados do usuário, do client, uid e token.
- axios: Biblioteca utilizada para fazer as requisições da Api.
- react-hook-form: Biblioteca utilizada para gerenciar os formulários na tela do login e na tela da filtragem de empresas.
- react-native-vector-icons: Biblioteca utiliza para adicionar ícones no aplicativo.
- styled-components: Biblioteca utilizada para a criação dos componentes.
- React-Navigation v6: Biblioteca utilizada para realizar o processo de navrgação entre as telas do aplicativo.

# Conclusão

Foi desenvolvido uma aplicação React Native, utilizando todos os endpoints disponíveis. Além disso, os formulários possuem validações e é possível deslogar o usuário do aplicativo.
